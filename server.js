const express = require("express")
const app = express()
const server = require('http').Server(app)
server.listen(80, '0.0.0.0')
const io = require('socket.io')(server)
const path = require('path')
const fs = require('fs')
const cors = require('cors')

var vote = false
var listMade = false
var hostIp = "100.64.31.161"

var canditateList = []
var voterList = []
var totalVotes = 0

app.set('views', './views')
app.set('view engine', 'ejs')
app.use(express.static('public'))
app.use(express.urlencoded({ extended: true }))
app.use(
    cors({
        origin: "*",
    })
)

app.get('/maker', (req, res) => {
  if (req.connection.remoteAddress == hostIp){
    res.render('maker');
  } else {
    res.redirect('/vote');
  }
})

app.get('/vote', (req,res) => {
  if(!vote){
    res.render('no-vote')
  } else {
    res.render('index', { canditateList : canditateList.sort((a,b) => a.votes - b.votes), totalVotes: totalVotes, votesData: getVotesData(), haveVoted : existInList(req.connection.remoteAddress, voterList) })
  }
})

app.get('*', (req, res) => {
  res.redirect('/vote')
})

io.on('connection', socket => {

  socket.on('vote', canditate => {
    if(!existInList(socket.conn.remoteAddress, voterList) && canditateExist(canditate, canditateList) && vote) {
      canditateList[getCanditateIndex(canditate, canditateList)].votes++;
      voterList.push(socket.conn.remoteAddress)
      totalVotes++;
    }
  })

  socket.on('list', list => {
    if(socket.conn.remoteAddress == hostIp && !listMade) {
      canditateList = list
      vote = true
      listMade = true
      io.emit('reload')
    }
  })

  socket.on('end-vote', () => {
    if(socket.conn.remoteAddress == hostIp){
      listMade = false
      vote = false
      canditateList = []
      voterList = []
      totalVotes = 0;
      io.emit('reload')
    }
  })
})

function existInList(item, list){
  var returnValue = false;
  list.forEach(listItem => {
    if (listItem == item){
      returnValue = true;
    }
  })
  return returnValue;
}

function canditateExist(canditate, list){
  var returnValue = false;
  list.forEach(listItem => {
    if (listItem.name == canditate){
      returnValue = true;
    }
  })
  return returnValue;
}

function getCanditateIndex(item, list){
  var returnValue = null;
  list.forEach(listItem => {
    if (listItem.name == item) {
    returnValue = list.indexOf(listItem)
    }
  })
  return returnValue;
}

function getVotesData(){
  var data = []

  canditateList.forEach(canditate => {
    data.push(canditate.votes)
  })

  return data;
}
