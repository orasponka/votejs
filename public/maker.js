var socket = io("http://100.64.31.161")
var canditateList = []
var listElement = document.getElementById("list");
var formElement = document.getElementById("form");
var canditateInput = document.getElementById("input");
var descInput = document.getElementById("desc");

formElement.addEventListener('submit', e => {
  e.preventDefault()
  const addedCanditate = canditateInput.value;
  const desc = descInput.value;
  if(desc != "" && addedCanditate != "" && !existInList(addedCanditate, canditateList)){
  canditateList.push({ name: addedCanditate, desc : desc, votes: 0 });
  addToListElement(addedCanditate);
  }
})

function addToListElement(text) {
  const listItem = document.createElement('li')
  listItem.innerText = text
  listItem.id = text
  listItem.onclick = function () { remove(text) }
  listItem.className = "list-group-item"
  listElement.append(listItem)
}

function existInList(item, list){
  var returnValue = false;
  list.forEach(listItem => {
    if (listItem.name == item){
      returnValue = true;
    }
  })
  return returnValue;
}

function sendList(){
  if(canditateList.length > 1){
    console.log(canditateList)
    socket.emit('list', canditateList)
    window.location = '/vote'
  }
}

socket.on('reload', () => {
  location.reload()
})

function reset(){
  location.reload()
}

function remove(name){
  var index;
  var element = document.getElementById(name);
  for (var i = 0; i < canditateList.length; i++){
    if (canditateList[i].name == name) {
      index = i
    }
  }
  element.parentNode.removeChild(element)
  canditateList = arrayRemove(canditateList, canditateList[index])
}

function arrayRemove(arr, value) {

  return arr.filter(function(ele){
    return ele != value;
  });
}
