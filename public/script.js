const socket = io("http://100.64.31.161")
const hideBtn = document.getElementById('hidebtn')
var firstTime = true

function vote(canditate){
  console.log(canditate)
  socket.emit("vote", canditate)
}

socket.on('reload', () => {
  location.reload()
})

function end(){
  socket.emit('end-vote')
}

function reload(){
  location.reload()
}

function hide(){
  var element = document.getElementById("canditateContainer")

  if(element.style.display == "block"){
    element.style.display = "none"
  } else {
    element.style.display = "block"
  }
}

hideBtn.addEventListener('click', () => {
  if(firstTime){
  hide()
  hide()
  firstTime = false
  } else {
    hide()
  }
})
